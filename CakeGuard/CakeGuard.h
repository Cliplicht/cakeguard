#pragma once

#include "targetver.h"
#include <iostream>
#include <Windows.h>
#include <stdio.h>
#include <tchar.h>
#include <string>
#include <cstdio>
#include <tlhelp32.h>
#include <thread>
#include <Psapi.h>
#include <list>
#include <cctype>
#include <sstream>


class CakeGuard
{
public:
	static void Init();
	static void logger(std::string logText);
	static void shutdown();
	static DWORD ProcID;
	static void outlook_proc();
private:
	static void watcher();
	static void handle_key_input(PKBDLLHOOKSTRUCT key);
	static const std::string console_prefix;
	
};
