// CakeGuard.cpp : Defines the entry point for the console application.
//

#include "CakeGuard.h"

const std::string CakeGuard::console_prefix = "[CakeGuard]";
DWORD CakeGuard::ProcID = 0;
HHOOK keyboardHook;
char cakeWord[] = {'C'};
DWORD LastKey = 'A';
NTSTATUS notifyProc;
bool processFound = false;


void CakeGuard::logger(const std::string log_text)
{
	std::cout << console_prefix + " > " + log_text << std::endl;
}

BOOL WINAPI ConsoleHandler(DWORD dwCtrlEvent)
{
	switch (dwCtrlEvent)
	{
	case CTRL_C_EVENT:
		CakeGuard::shutdown();
	case CTRL_BREAK_EVENT:
		CakeGuard::shutdown();
	case CTRL_CLOSE_EVENT:
		CakeGuard::shutdown();
	case CTRL_LOGOFF_EVENT:
		CakeGuard::shutdown();
	case CTRL_SHUTDOWN_EVENT:
		CakeGuard::shutdown();
	default:
		return FALSE;
	}
}

int main()
{
	CakeGuard::Init();
	std::cout << std::endl;
	MSG msg{0};
	//our application loop
	while (GetMessage(&msg, NULL, 0, 0) != 0);


	return 0;
}


void SearchOutlook()
{
	while (true)
	{
		if (CakeGuard::ProcID == 0)
		{
			processFound = false;
		}

		if (CakeGuard::ProcID == 0 && processFound != true)
		{
			processFound = true;
			CakeGuard::outlook_proc();
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		}
	}
}

void CakeGuard::Init()
{
	SetConsoleTitle(L"CakeGuard");
	if (SetConsoleCtrlHandler((PHANDLER_ROUTINE)ConsoleHandler,TRUE) == FALSE)
	{
		// unable to install handler... 
		// display message to the user
		printf("Unable to install handler!\n");
	}
	::ShowWindow(::GetConsoleWindow(),SW_HIDE);
	logger("CakeGuard is your best Friend!");
	std::thread outlookSearch(SearchOutlook);
	outlookSearch.detach();
	watcher();
}

bool UserTypedCake(DWORD vKey)
{
	if (vKey == 'C' && cakeWord[0] != 'K' && cakeWord[1] != 'U')
	{
		cakeWord[0] = 'C';
		LastKey = vKey;
	}
	else if (vKey == 'A' && LastKey == 'C')
	{
		cakeWord[1] = 'A';
		LastKey = vKey;
	}
	else if (vKey == 'K' && LastKey == 'A')
	{
		cakeWord[2] = 'K';
		LastKey = vKey;
	}
	else if (vKey == 'E' && LastKey == 'K')
	{
		cakeWord[3] = 'E';
		LastKey = vKey;
		return true;
	}


	return false;
}

bool UserTypedCakeGerman(DWORD vKey)
{
	if (vKey == 'K')
	{
		cakeWord[0] = 'K';
		LastKey = vKey;
	}
	else if (vKey == 'U' && LastKey == 'K' && cakeWord[0] == 'K')
	{
		cakeWord[1] = 'U';
		LastKey = vKey;
	}
	else if (vKey == 'C' && LastKey == 'U' && cakeWord[1] == 'U')
	{
		cakeWord[2] = 'C';
		LastKey = vKey;
	}
	else if (vKey == 'H' && LastKey == 'C' && cakeWord[2] == 'C')
	{
		cakeWord[3] = 'H';
		LastKey = vKey;
	}
	else if (vKey == 'E' && LastKey == 'H' && cakeWord[3] == 'H')
	{
		cakeWord[4] = 'E';
		LastKey = vKey;
	}
	else if (vKey == 'N' && LastKey == 'E' && cakeWord[4] == 'E')
	{
		cakeWord[5] = 'N';
		LastKey = vKey;

		return true;
	}


	return false;
}

void handle_key_input(PKBDLLHOOKSTRUCT key)
{
	char alpha = static_cast<char>(key->vkCode);

	if (alpha == VK_BACK)
	{
		//CakeGuard::logger("BACK");
	}
	else if (alpha == VK_SHIFT)
	{
	}
	else
	{
		if (UserTypedCake(key->vkCode) || UserTypedCakeGerman(key->vkCode))
		{
			if (CakeGuard::ProcID != 0)
			{
				LockWorkStation();
				const HANDLE outlook = OpenProcess(PROCESS_ALL_ACCESS,TRUE,CakeGuard::ProcID);
				TerminateProcess(outlook, 0);
				CloseHandle(outlook);
				
			}
		}
		/*std::stringstream alpha_ss;
		std::string alpha_str;
		alpha_ss << alpha;
		alpha_ss >> alpha_str;
		CakeGuard::logger(alpha_str);*/
	}
}

LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	PKBDLLHOOKSTRUCT key = (PKBDLLHOOKSTRUCT)lParam;
	//a key was pressed
	if (wParam == WM_KEYDOWN && nCode == HC_ACTION)
	{
		handle_key_input(key);
	}
	return CallNextHookEx(keyboardHook, nCode, wParam, lParam);
}

void CakeGuard::watcher()
{
	keyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardProc, NULL, NULL);
}

void ProcRunning()
{
	while (true)
	{
		HANDLE proc = OpenProcess(PROCESS_ALL_ACCESS, 0, CakeGuard::ProcID);
		DWORD exitCode = 0;
		if (GetExitCodeProcess(proc, &exitCode) == FALSE)
		{
			CakeGuard::logger("proccess not running or closed;");
		}
		else if (exitCode != STILL_ACTIVE)
		{
			CakeGuard::ProcID = 0;
			processFound = false;
			CloseHandle(proc);
			break;
		}
		CloseHandle(proc);
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
}

void CakeGuard::outlook_proc()
{
	PROCESSENTRY32 pe32;
	HANDLE hSnapshot = NULL;

	pe32.dwSize = sizeof(PROCESSENTRY32);
	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (Process32First(hSnapshot, &pe32))
	{
		do
		{
			/*std::wstring ws(pe32.szExeFile);
			std::string txt(ws.begin(),ws.end());
			logger(txt);*/
			if (wcscmp(pe32.szExeFile, L"OUTLOOK.EXE") == 0)
			{
				if (ProcID != pe32.th32ProcessID)
				{
					ProcID = pe32.th32ProcessID;
					logger("outlook process found: " + std::to_string(ProcID));
					std::thread procRunningThread(ProcRunning);
					procRunningThread.detach();
				}
				break;
			}
		}
		while (Process32Next(hSnapshot, &pe32));
	}

	if (hSnapshot != INVALID_HANDLE_VALUE)
		CloseHandle(hSnapshot);
}

void CakeGuard::shutdown()
{
	UnhookWindowsHookEx(keyboardHook);
}
